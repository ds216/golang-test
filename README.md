# Golang Test

Testing Material for leaning Go

Things to learn / Test
*  HTTP request handling
*  Templates
*  Read / Write a JSON file
*  Connect to Database 

Targets
*  Migrate Current Portfolio from PHP to Go
*  REST API develop
*  JSON package
*  deploy to Google Cloud Platform
*  [Extra] Database support (Mongo DB / MySQL)

# Materials

* Reading query string from url - https://www.jianshu.com/p/1075211f0556
* Go Templates Example - https://gowebexamples.com/templates/
* Go Template Best Partices - https://colobu.com/2016/10/09/Go-embedded-template-best-practices/
* From PHP to Go - https://yami.io/php-to-golang/
* build-web-application-with-golang - https://github.com/astaxie/build-web-application-with-golang/blob/master/en/01.0.md
* How to deploy Go Program to GCP - https://medium.com/@mingderwang/%E5%A6%82%E4%BD%95%E5%9C%A8-google-cloud-%E5%AF%AB%E5%92%8C%E9%83%A8%E7%BD%B2-go-%E7%A8%8B%E5%BC%8F-f8518414a30e
* Go on GCP - https://cloud.google.com/go/home

# Further Study
* Server-side I/O Performance: Node vs. PHP vs. Java vs. Go  - https://www.toptal.com//back-end/server-side-io-performance-node-php-java-go