package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
)

/*
Original Code:
https://golang.org/doc/articles/wiki/final.go
Modified for testing and learning
*/

//Handler to load index template
func indexHandler(w http.ResponseWriter, r *http.Request, title string) {
	//p, err := loadPage(title)
	//if err != nil {
	//	http.Redirect(w, r, "/"+title, http.StatusFound)
	//	return
	//}
	renderTemplate(w, "index", "index", nil)
}

//Handler to load test template
func testHandler(w http.ResponseWriter, r *http.Request, title string) {
	// TODO : parameters??
	m := make(map[string]string) //Declare a map m
	headerStr := ""
	header := r.Header
	for k, v := range header {
		headerStr += "\n" + k + " = " + strings.Join(v, ",")
	}
	m["header"] = headerStr //use = here not := for insert item to map
	body, err := ioutil.ReadAll(r.Body)
	if err != nil { //error handling
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	m["body"] = string(body) //convert byte to string
	renderTemplate(w, "test", headerStr, m)
}

//List of Available Temple
var templates = template.Must(template.ParseFiles("index.html", "test.html"))

//Get the template, if no template found than 500
func renderTemplate(w http.ResponseWriter, tmpl string, text string, m map[string]string) {
	var err error //declare a empty error Obj err
	if m != nil {
		err = templates.ExecuteTemplate(w, tmpl+".html", m)
	} else {
		err = templates.ExecuteTemplate(w, tmpl+".html", template.HTML(text))
	}
	if err != nil { //error handling
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Regular Expression to check if URL accessed is valid
var validPath = regexp.MustCompile("^/(testA|)$")

// closure to encloses values defined outside, fn is enclosed
func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, "test") //passing "test" to handler
	}
}

func main() {
	// mapping of URL and handler
	http.HandleFunc("/testA", makeHandler(testHandler))
	http.HandleFunc("/", makeHandler(indexHandler))
	//bound the web serve to localhost port 8080
	log.Fatal(http.ListenAndServe(":8080", nil))
}
